//
//  MAIKit-macOS.h
//  MAIKit-macOS
//
//  Created by Bonk, Thomas on 31.01.17.
//
//

#import <Cocoa/Cocoa.h>

//! Project version number for MAIKit-macOS.
FOUNDATION_EXPORT double MAIKit_macOSVersionNumber;

//! Project version string for MAIKit-macOS.
FOUNDATION_EXPORT const unsigned char MAIKit_macOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MAIKit_macOS/PublicHeader.h>


