//
//  MAIKit-iOS.h
//  MAIKit-iOS
//
//  Created by Bonk, Thomas on 31.01.17.
//
//

#import <UIKit/UIKit.h>

//! Project version number for MAIKit-iOS.
FOUNDATION_EXPORT double MAIKit_iOSVersionNumber;

//! Project version string for MAIKit-iOS.
FOUNDATION_EXPORT const unsigned char MAIKit_iOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MAIKit_iOS/PublicHeader.h>


